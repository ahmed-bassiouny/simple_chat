package com.kproject.simplechat.data.utils

import java.util.*

// this for simulate user it
// dummy class not related with any structure
object Utils {

    private var userId = 0L

    fun getUserId():String {
        if(userId == 0L)
            userId = Calendar.getInstance().timeInMillis
        return userId.toString()
    }

}