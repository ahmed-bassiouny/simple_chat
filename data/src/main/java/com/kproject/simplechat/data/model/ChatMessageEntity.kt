package com.kproject.simplechat.data.model


data class ChatMessageEntity(
    val message: String = "",
    val senderId: String = "",
    val typing: Boolean = false,
    val channel: String = ""
)

enum class MessageChannel(val value: String) {
    MESSAGE("message"), TYPING("typing")
}