import com.kproject.simplechat.Versions
import com.kproject.simplechat.Dependencies
import com.kproject.simplechat.Android

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    //id("dagger.hilt.android.plugin")
    //id("com.google.gms.google-services")
}

android {
    compileSdk = Android.compileSdk

    defaultConfig {
        applicationId = "com.kproject.simplechat"
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk
        versionCode = Android.versionCode
        versionName = Android.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }


    packagingOptions {
        resources.excludes.add("/META-INF/{AL2.0,LGPL2.1}")
    }
    namespace = "com.kproject.simplechat"

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    // Modules
    implementation(project(Dependencies.Module.domain))
    implementation(project(Dependencies.Module.componentNetwork))
    implementation(project(Dependencies.Module.componentRepository))
    implementation(project(Dependencies.Module.data))

    implementation(Dependencies.coreKtx)
    implementation(Dependencies.lifecycleRuntime)
    implementation(Dependencies.lifecycleExtensions)
    implementation(Dependencies.lifecycleViewModel)
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation(Dependencies.koinCore)
    implementation(Dependencies.koinAndroid)
    implementation(Dependencies.kotlinxCoroutinesAndroid)
    implementation(Dependencies.gson)
    implementation(Dependencies.websocket)
    implementation("androidx.recyclerview:recyclerview:1.3.2")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
}