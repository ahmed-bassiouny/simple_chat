package com.kproject.simplechat

import android.app.Application
import com.kproject.simplechat.di.AppKoinModule
import com.kproject.simplechat.domain.di.DomainKoinModule
import com.online.component_network.data.di.NetworkKoinModule
import com.online.component_repository.di.RepositoryKoinModule
import org.koin.core.context.startKoin


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(
                listOf(

                    /**
                     * Change Feature A Impl with a single DI line.
                     */
                    NetworkKoinModule.create(),
                    DomainKoinModule.create(),
                    RepositoryKoinModule.create(),
                    AppKoinModule.create(),
                    )
            )
        }

    }
}