package com.kproject.simplechat.di


import com.kproject.simplechat.presentation.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object AppKoinModule {
    fun create(): Module {
        return module {
            viewModel { MainViewModel(get(), get(), get(), get(), get(), get(), get()) }
        }
    }
}
