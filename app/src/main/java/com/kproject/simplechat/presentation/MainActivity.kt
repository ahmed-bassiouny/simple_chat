package com.kproject.simplechat.presentation


import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import com.kproject.simplechat.databinding.ActivityMainBinding
import com.kproject.simplechat.helper.TypingListener
import com.kproject.simplechat.presentation.adapter.ChatAdapter
import com.kproject.simplechat.presentation.mapper.MessageUIModel
import kotlinx.coroutines.delay
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {


    private val myViewModel: MainViewModel by viewModel()
    private var adapter: ChatAdapter? = null
    private var binding:ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        initViews()
        observers()
        listeners()

//        ArrayList<MessageUIModel>().apply {
//            add(MessageUIModel("ahmed",true))
//            add(MessageUIModel("ahmed",true))
//            add(MessageUIModel("ahmed",true))
//            add(MessageUIModel("ahmed",true))
//            adapter?.submitList(this)
//        }
    }

    private fun listeners() {

        binding?.etMessage?.addTextChangedListener(TypingListener { typingStatus ->
            myViewModel.changeStatus(typingStatus)
        })

        binding?.btnSend?.setOnClickListener {
            myViewModel.sendMessage(binding?.etMessage?.text.toString())
        }
    }

    private fun initViews() {
        adapter = ChatAdapter()
        binding?.recycler?.adapter = adapter
    }

    private fun observers() {
        myViewModel.clearInput.observe(this){
            binding?.etMessage?.text?.clear()
        }
        myViewModel.messagesObserver.observe(this) {
            adapter?.submitList(it.toMutableList())
        }
        myViewModel.statusSocketObserver.observe(this) {
            binding?.btnSend?.isEnabled = it
            binding?.btnSend?.text = getSendButtonText(it)
        }

        myViewModel.isUserTyping.observe(this) {
            binding?.tvTyping?.isVisible = it
        }
    }

    private fun getSendButtonText(connectionStatus:Boolean) = if(connectionStatus) "Send" else "ConnectionLost"

}