package com.kproject.simplechat.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kproject.simplechat.databinding.ItemChatBinding
import com.kproject.simplechat.presentation.mapper.MessageUIModel

class ChatAdapter :
    ListAdapter<MessageUIModel, RecyclerView.ViewHolder>(ChatDiffCallBack()) {

    inner class ChatViewHolder(val binding: ItemChatBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(item: MessageUIModel) {
            binding.message.text = item.message
            binding.llContainer.layoutDirection =
                if (item.isFromMe) View.LAYOUT_DIRECTION_RTL else View.LAYOUT_DIRECTION_LTR
        }
    }

    class ChatDiffCallBack : DiffUtil.ItemCallback<MessageUIModel>() {
        override fun areItemsTheSame(oldItem: MessageUIModel, newItem: MessageUIModel): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: MessageUIModel, newItem: MessageUIModel): Boolean =
            oldItem == newItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // your implementation
        return ChatViewHolder(
            ItemChatBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ChatViewHolder)?.onBind(getItem(position))
    }
}


