package com.kproject.simplechat.presentation

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kproject.simplechat.data.utils.Utils
import com.kproject.simplechat.domain.usecase.SocketChangeTypingStatus
import com.kproject.simplechat.domain.usecase.SocketConnect
import com.kproject.simplechat.domain.usecase.SocketOnConnected
import com.kproject.simplechat.domain.usecase.SocketOnDisconnected
import com.kproject.simplechat.domain.usecase.SocketOnMessage
import com.kproject.simplechat.domain.usecase.SocketOnTyping
import com.kproject.simplechat.domain.usecase.SocketSendMessage
import com.kproject.simplechat.presentation.mapper.MessageUIModel
import com.kproject.simplechat.presentation.mapper.toMessageUIModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

private const val TAG = "MainViewModel"


class MainViewModel constructor(
    private val socketOnMessage: SocketOnMessage,
    private val socketOnTyping: SocketOnTyping,
    private val socketOnConnected: SocketOnConnected,
    private val socketOnDisconnected: SocketOnDisconnected,
    private val socketConnect: SocketConnect,
    private val socketChangeTypingStatus: SocketChangeTypingStatus,
    private val socketSendMessage: SocketSendMessage,
) : ViewModel() {

    val statusSocketObserver = MutableLiveData<Boolean>()
    val isUserTyping = MutableLiveData<Boolean>()
    val messagesObserver = MutableLiveData<ArrayList<MessageUIModel>>()
    val list = ArrayList<MessageUIModel>()
    val clearInput = MutableLiveData<Unit>()

    init {
        reactOnConnectedSocket()
        reactOnDisconnectedSocket()
        reactOnNewMessage()
        reactOnTyping()
        startConnection()
    }

    private fun reactOnConnectedSocket() = viewModelScope.launch {
        socketOnConnected.execute(Unit).collect {
            statusSocketObserver.postValue(true)
        }
    }

    private fun reactOnTyping() = viewModelScope.launch {
        socketOnTyping.execute(Unit).collect {
            Log.e(TAG, "reactOnTyping: ", )
            isUserTyping.postValue(it)
        }
    }

    private fun reactOnDisconnectedSocket() = viewModelScope.launch {
        socketOnDisconnected.execute(Unit).collect {
            statusSocketObserver.postValue(false)
        }
    }

    private fun reactOnNewMessage() = viewModelScope.launch {
        socketOnMessage.execute(Unit).collect {
            Log.e(TAG, "reactOnNewMessage: ")
            list.add(it.toMessageUIModel(Utils.getUserId()))
            messagesObserver.postValue(list)
        }
    }

    private fun startConnection() = viewModelScope.launch {
        socketConnect.execute(Unit)
    }

    fun changeStatus(status: Boolean) = viewModelScope.launch {
        socketChangeTypingStatus.execute(status)
    }

    fun sendMessage(message: String) = viewModelScope.launch {
        socketSendMessage.execute(message)
        clearInput.postValue(Unit)
    }

}