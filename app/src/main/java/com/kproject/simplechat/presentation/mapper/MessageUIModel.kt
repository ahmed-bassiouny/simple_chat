package com.kproject.simplechat.presentation.mapper

import com.kproject.simplechat.data.model.ChatMessageEntity

data class MessageUIModel(
    val message:String,
    val isFromMe:Boolean
)


fun ChatMessageEntity.toMessageUIModel(userId:String) = MessageUIModel(message,senderId == userId)