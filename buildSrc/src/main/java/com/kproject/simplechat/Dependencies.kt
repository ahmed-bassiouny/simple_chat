package com.kproject.simplechat

object Dependencies {
    object Module {
        const val domain = ":domain"
        const val data = ":data"
        const val componentNetwork = ":component-network"
        const val componentRepository = ":component-repository"
    }

    object Plugin {

        const val kotlin = "org.jetbrains.kotlin.android"
        const val kapt = "kotlin-kapt"

    }

    const val coreKtx = "androidx.core:core-ktx:${Versions.CORE_KTX}"
    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.LIFECYCLE_RUNTIME_KTX}"
    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.LIFECYCLE_EXTENSIONS}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel:${Versions.LIFECYCLE_VIEWMODEL_KTX}"


    const val googleServices = "com.google.gms:google-services:${Versions.GOOGLE_SERVICES}"



    // Coroutines
    const val kotlinxCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.KOTLIN_COROUTINES}"
    const val kotlinxCoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.KOTLIN_COROUTINES}"
    const val kotlinxCoroutinesPlayServices = "org.jetbrains.kotlinx:kotlinx-coroutines-play-services:${Versions.KOTLIN_COROUTINES_PLAY_SERVICES}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}"

    // Gson
    const val gson = "com.google.code.gson:gson:${Versions.GSON}"



    // koin
    const val koinCore = "io.insert-koin:koin-core:${Versions.KOIN}"
    const val koinAndroid = "io.insert-koin:koin-android:${Versions.KOIN}"

    const val websocket = "io.ktor:ktor-websockets:${Versions.WEB_SOCKET}"


}