package com.kproject.simplechat

object Versions {
    const val KOTLIN = "1.8.0"

    const val CORE_KTX = "1.8.0"
    const val LIFECYCLE_RUNTIME_KTX = "2.5.1"
    const val LIFECYCLE_EXTENSIONS= "2.2.0"
    const val LIFECYCLE_VIEWMODEL_KTX = "2.5.0"

    const val CONSTRAINT_LAYOUT_COMPOSE = "1.0.1"


    const val GOOGLE_SERVICES = "4.3.13"


    // Coroutines
    const val KOTLIN_COROUTINES = "1.6.4"
    const val KOTLIN_COROUTINES_PLAY_SERVICES = "1.6.4"




    const val RETROFIT = "2.9.0"
    const val GSON = "2.9.1"

    const val SPLASH_SCREEN = "1.0.0"

    const val KOIN = "3.2.0"

    const val WEB_SOCKET = "1.6.7"

    
}

