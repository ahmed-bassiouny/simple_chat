package com.kproject.simplechat.domain.usecase


import com.kproject.simplechat.data.model.ChatMessageEntity
import com.kproject.simplechat.data.model.MessageChannel
import com.kproject.simplechat.data.utils.Utils
import com.kproject.simplechat.domain.usecase.baseusecase.FlowUseCase
import com.online.component_repository.SocketRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map

class SocketOnTyping constructor(
    private val socketRepository: SocketRepository,
) : FlowUseCase<Unit, Boolean> {

    override fun execute(request: Unit): Flow<Boolean> =
        socketRepository.onMessage()
            .filter { it.channel == MessageChannel.TYPING.value && it.senderId != Utils.getUserId() }
            .map { it.typing }
}