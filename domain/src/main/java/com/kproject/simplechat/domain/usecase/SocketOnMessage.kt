package com.kproject.simplechat.domain.usecase


import com.kproject.simplechat.data.model.ChatMessageEntity
import com.kproject.simplechat.data.model.MessageChannel
import com.kproject.simplechat.domain.usecase.baseusecase.FlowUseCase
import com.online.component_repository.SocketRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter

class SocketOnMessage constructor(
    private val socketRepository: SocketRepository,
): FlowUseCase<Unit, ChatMessageEntity> {

    override fun execute(request: Unit): Flow<ChatMessageEntity> =
        socketRepository.onMessage().filter { it.channel == MessageChannel.MESSAGE.value }
}