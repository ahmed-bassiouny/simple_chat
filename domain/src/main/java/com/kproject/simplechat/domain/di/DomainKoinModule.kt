package com.kproject.simplechat.domain.di

import com.kproject.simplechat.domain.usecase.*
import org.koin.core.module.Module
import org.koin.dsl.module

object DomainKoinModule {
    fun create(): Module {
        return module {
            single { SocketConnect(get()) }
            single { SocketDisconnect(get()) }
            single { SocketOnConnected(get()) }
            factory { SocketOnDisconnected(get()) }
            factory { SocketOnMessage(get()) }
            factory { SocketOnTyping(get()) }
            single { SocketSendMessage(get()) }
            single { SocketChangeTypingStatus(get()) }

        }
    }
}