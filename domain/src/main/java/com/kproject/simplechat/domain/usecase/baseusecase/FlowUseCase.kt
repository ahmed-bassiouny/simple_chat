package com.kproject.simplechat.domain.usecase.baseusecase

import kotlinx.coroutines.flow.Flow

interface FlowUseCase<REQUEST, RESPONSE> {
    fun execute(request: REQUEST): Flow<RESPONSE>
}