package com.kproject.simplechat.domain.usecase


import com.kproject.simplechat.data.model.ChatMessageEntity
import com.kproject.simplechat.data.model.MessageChannel
import com.kproject.simplechat.data.utils.Utils
import com.kproject.simplechat.domain.usecase.baseusecase.UseCase
import com.online.component_repository.SocketRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter

class SocketSendMessage constructor(
    private val socketRepository: SocketRepository,
) : UseCase<String, Unit> {

    override suspend fun execute(request: String) {
        if (request.isNotBlank() && request.isNotEmpty()) {
            socketRepository.sendMessage(
                ChatMessageEntity(
                    message = request,
                    senderId = Utils.getUserId(),
                    channel = MessageChannel.MESSAGE.value
                )
            )
        }
    }
}