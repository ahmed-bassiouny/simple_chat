package com.kproject.simplechat.domain.usecase.baseusecase


interface UseCase<REQUEST, RESPONSE> {

    suspend fun execute(request: REQUEST): RESPONSE

    suspend fun <T> wrap(or: UseCase<Unit, Unit>, block: suspend () -> T): Response<T> = try {
        Response.Success(block.invoke())
    }  catch (exception: Exception) {
        Response.Error.Generic(exception.message.toString())
    }

    suspend fun <T> wrap(block: suspend () -> T): Response<T> = try {
        Response.Success(block.invoke())
    }catch (exception: Exception) {
        Response.Error.Generic(exception.message.toString())
    }

    sealed class Response<T> {
        data class Success<T>(val value: T): Response<T>()
        sealed class Error<T>(open val error: String): Response<T>() {
            data class Generic<T>(override val error: String) : Error<T>(error)
        }
    }
}