package com.kproject.simplechat.domain.usecase


import com.kproject.simplechat.data.model.ChatMessageEntity
import com.kproject.simplechat.data.model.MessageChannel
import com.kproject.simplechat.data.utils.Utils
import com.kproject.simplechat.domain.usecase.baseusecase.UseCase
import com.online.component_repository.SocketRepository

class SocketChangeTypingStatus constructor(
    private val socketRepository: SocketRepository,
) : UseCase<Boolean, Unit> {

    override suspend fun execute(request: Boolean) =
        socketRepository.sendMessage(
            ChatMessageEntity(
                typing = request,
                senderId = Utils.getUserId(),
                channel = MessageChannel.TYPING.value
            )
        )
}