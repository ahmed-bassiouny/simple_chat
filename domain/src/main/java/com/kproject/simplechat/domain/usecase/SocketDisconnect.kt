package com.kproject.simplechat.domain.usecase

import com.kproject.simplechat.domain.usecase.baseusecase.UseCase
import com.online.component_repository.SocketRepository

class SocketDisconnect constructor(
    private val socketRepository: SocketRepository,
): UseCase<Unit, Unit> {

    override suspend fun execute(request: Unit) =
        socketRepository.disconnect()
}
