package com.kproject.simplechat.domain.usecase

import android.util.Log
import com.kproject.simplechat.data.BuildConfig
import com.kproject.simplechat.domain.usecase.baseusecase.UseCase
import com.online.component_repository.SocketRepository


class SocketConnect constructor(
    private val socketRepository: SocketRepository,
): UseCase<Unit, Unit> {

    override suspend fun execute(request: Unit) {
        socketRepository.connect(BuildConfig.socketURL)
    }
}