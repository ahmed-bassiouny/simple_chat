package com.kproject.simplechat.domain.usecase

import com.kproject.simplechat.domain.usecase.baseusecase.FlowUseCase
import com.online.component_repository.SocketRepository
import kotlinx.coroutines.flow.Flow

class SocketOnDisconnected constructor(
    private val socketRepository: SocketRepository,
): FlowUseCase<Unit, Unit> {

    override fun execute(request: Unit): Flow<Unit> =
        socketRepository.onDisconnected()
}