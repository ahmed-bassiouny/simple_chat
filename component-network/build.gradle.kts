import com.kproject.simplechat.Dependencies.Plugin.kapt

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.online.component_network"
    compileSdk = com.kproject.simplechat.Android.compileSdk

    defaultConfig {


        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {


    implementation(project(com.kproject.simplechat.Dependencies.Module.data))

    implementation(com.kproject.simplechat.Dependencies.coreKtx)
    implementation(com.kproject.simplechat.Dependencies.kotlinxCoroutinesAndroid)
    implementation(com.kproject.simplechat.Dependencies.kotlinxCoroutinesPlayServices)
    implementation(com.kproject.simplechat.Dependencies.retrofit)
    implementation(com.kproject.simplechat.Dependencies.gson)
    implementation(com.kproject.simplechat.Dependencies.koinCore)
}