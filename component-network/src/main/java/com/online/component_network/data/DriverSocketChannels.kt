package com.kproject.simplechat.data.utils

object DriverSocketChannels {

    enum class Receive(val event: String) {
        MESSAGES(event = "demo"),
    }
    fun listenerChannels(): List<String> =
        Receive.values().map { it.event }
}