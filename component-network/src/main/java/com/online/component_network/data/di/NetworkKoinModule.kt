package com.online.component_network.data.di


import com.google.gson.Gson
import com.online.component_network.socket.SocketEmitter
import com.online.component_network.socket.SocketManager
import com.online.component_network.socket.SocketMessageParser
import com.online.component_network.socket.SocketNetwork
import com.online.component_network.socket.SocketNetworkImpl
import okhttp3.OkHttpClient
import org.koin.core.module.Module
import org.koin.dsl.module


object NetworkKoinModule {

    fun create(): Module {
        return module {
            single { Gson() }
            single { OkHttpClient() }
            single { SocketMessageParser(get()) }
            single { SocketEmitter(get()) }
            single { SocketManager(get(),get()) }
            single<SocketNetwork> { SocketNetworkImpl(get()) }

        }
    }
}