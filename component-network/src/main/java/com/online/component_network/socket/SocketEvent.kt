package com.online.component_network.socket

sealed class SocketEvent {


    object NONE: SocketEvent() {
        override fun toString(): String = "NONE"
    }

    object DISCONNECTED: SocketEvent() {
        override fun toString(): String = "DISCONNECTED"
    }

    object CONNECTED: SocketEvent() {
        override fun toString(): String = "CONNECTED"
    }

}
