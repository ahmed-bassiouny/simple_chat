package com.online.component_network.socket

import com.kproject.simplechat.data.model.ChatMessageEntity
import kotlinx.coroutines.flow.Flow

interface SocketNetwork {

    suspend fun connect(socketUrl: String)
    suspend fun disconnect()
    fun onConnected(): Flow<Unit>
    fun onDisconnected(): Flow<Unit>
    fun onEvent(): Flow<String>
    fun onMessage(): Flow<ChatMessageEntity>
    fun sendMessage(item:ChatMessageEntity)
}