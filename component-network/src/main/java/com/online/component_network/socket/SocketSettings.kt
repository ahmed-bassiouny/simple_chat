package com.online.component_network.socket

data class SocketSettings(
    val channels: List<String>,
    val url: String,
)
