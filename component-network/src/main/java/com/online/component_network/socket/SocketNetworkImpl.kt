package com.online.component_network.socket

import com.kproject.simplechat.data.model.ChatMessageEntity
import com.kproject.simplechat.data.utils.DriverSocketChannels
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map

class SocketNetworkImpl constructor(
    private val socketManager: SocketManager,
): SocketNetwork {
    override suspend fun connect(socketUrl: String) {
        socketManager.connect(SocketSettings(DriverSocketChannels.listenerChannels(), socketUrl))
    }

    override suspend fun disconnect() {
        socketManager.disconnect()
    }

    override fun onConnected()= socketManager.socketEmitter().onEvent.filter { it == SocketEvent.CONNECTED }.map {}
    override fun onDisconnected()= socketManager.socketEmitter().onEvent.filter { it == SocketEvent.DISCONNECTED }.map {}

    override fun onEvent()  =
        socketManager.socketEmitter().onEvent.map { it.toString() }

    override fun onMessage() : Flow<ChatMessageEntity> =
        socketManager.socketEmitter().onMessageEvent(ChatMessageEntity::class)

    override fun sendMessage(item: ChatMessageEntity) {
        val content = socketManager.socketEmitter().messageParser.parse(item)
        socketManager.emit(content)
        socketManager.publishReceivedMessage(content)
    }

}