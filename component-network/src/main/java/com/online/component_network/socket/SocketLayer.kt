package com.online.component_network.socket

enum class SocketLayer(val title: String) {
    WS(title = "websocket")
}
