package com.online.component_network.socket

data class SocketMessage(
    val content: String,
)
