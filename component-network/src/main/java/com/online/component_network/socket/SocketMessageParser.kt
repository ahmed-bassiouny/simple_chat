package com.online.component_network.socket

import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException


class SocketMessageParser constructor(private val gson: Gson) {

    fun <T> parse(json: String, responseType: Class<T>): T? = try {
        gson.fromJson(json, responseType)
    } catch (e: JsonParseException) {
        null
    } catch (e: JsonSyntaxException) {
        null
    }

    fun <T> parse(json: T) = gson.toJson(json)
}
