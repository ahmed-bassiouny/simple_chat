package com.online.component_network.socket

import kotlinx.coroutines.flow.*


import kotlin.reflect.KClass

class SocketEmitter constructor(val messageParser: SocketMessageParser) {

    val onEvent: MutableStateFlow<SocketEvent> = MutableStateFlow(SocketEvent.NONE)
    val onMessageEvent: MutableSharedFlow<SocketMessage> = MutableSharedFlow()

    fun <T: Any>onMessageEvent(responseType: KClass<T>): Flow<T> = onMessageEvent
        .map { messageParser.parse(it.content, responseType.java) }
        .filterNotNull()
}
