package com.online.component_network.socket

interface SocketConfiguration {
    val layers: List<SocketLayer>
    val isSecureConnection: Boolean
}
