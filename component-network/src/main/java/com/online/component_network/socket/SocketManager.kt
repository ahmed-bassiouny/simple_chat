package com.online.component_network.socket



import kotlinx.coroutines.runBlocking
import okhttp3.*


class SocketManager constructor(
    private val okHttpClient: OkHttpClient,
    private val socketEmitter: SocketEmitter,
) {


    private var webSocket: WebSocket? = null


    fun connect(settings: SocketSettings) {
        if (isConnected().not()) attemptConnection(settings)
    }

    fun disconnect() {
        if (isConnected()) attemptDisconnection()
    }

    fun emit(message: String) {
        webSocket?.send(message)
    }

    private fun attemptConnection(settings: SocketSettings) {
        createNewSocket(settings)
    }

    private fun socketRequest(settings: SocketSettings) = Request.Builder()
        .url(settings.url)
        .build()

    private fun attemptDisconnection() {
        webSocket?.cancel()
        webSocket = null
    }

    private fun createNewSocket(settings: SocketSettings) {
        webSocket = okHttpClient.newWebSocket(socketRequest(settings), CustomWebSocketListener())
    }


    private fun fireEvent(event: SocketEvent) = runBlocking {
        socketEmitter.onEvent.emit(event)
    }

    private fun fireEvent(message: SocketMessage) = runBlocking {
        socketEmitter.onMessageEvent.emit(message)
    }


    private fun onConnectedEvent() {
        fireEvent(SocketEvent.CONNECTED)
    }


    private fun onDisconnectedEvent() {
        fireEvent(SocketEvent.DISCONNECTED)
    }

    private fun onConnectionError() {
        fireEvent(SocketEvent.DISCONNECTED)
    }


    private fun onMessageReceived(content: String) = publishReceivedMessage(content)


     fun publishReceivedMessage(content: String) {
        fireEvent(SocketMessage(content))
    }

    private fun isConnected() =
        socketEmitter.onEvent.value is SocketEvent.CONNECTED

    fun socketEmitter() = socketEmitter

    inner class CustomWebSocketListener: WebSocketListener() {


        override fun onOpen(webSocket: WebSocket, response: Response) {
            super.onOpen(webSocket, response)
            onConnectedEvent()
        }


        override fun onMessage(webSocket: WebSocket, text: String) {
            super.onMessage(webSocket, text)
            onMessageReceived(text)
        }


        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            super.onClosed(webSocket, code, reason)
            onDisconnectedEvent()
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            super.onFailure(webSocket, t, response)
            onConnectionError()
        }

    }
}
