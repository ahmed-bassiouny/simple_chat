plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.online.component_repository"
    compileSdk = com.kproject.simplechat.Android.compileSdk

    defaultConfig {

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {


    implementation(project(com.kproject.simplechat.Dependencies.Module.componentNetwork))
    implementation(project(com.kproject.simplechat.Dependencies.Module.data))
    implementation(com.kproject.simplechat.Dependencies.kotlinxCoroutinesAndroid)
    implementation(com.kproject.simplechat.Dependencies.koinCore)

}