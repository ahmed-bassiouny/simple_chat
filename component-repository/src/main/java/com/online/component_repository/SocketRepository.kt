package com.online.component_repository

import com.kproject.simplechat.data.model.ChatMessageEntity
import kotlinx.coroutines.flow.Flow

interface SocketRepository {
    suspend fun connect(socketUrl: String)
    suspend fun disconnect()
    fun onConnected(): Flow<Unit>
    fun onDisconnected(): Flow<Unit>
    fun onEvent(): Flow<String>
    fun onMessage(): Flow<ChatMessageEntity>
    fun sendMessage(item:ChatMessageEntity)
}