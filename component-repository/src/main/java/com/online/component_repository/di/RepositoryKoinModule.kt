package com.online.component_repository.di


import com.online.component_repository.SocketRepository
import com.online.component_repository.SocketRepositoryImpl
import org.koin.core.module.Module
import org.koin.dsl.module


object RepositoryKoinModule {
    fun create(): Module {
        return module {
            single<SocketRepository> { SocketRepositoryImpl(get()) }
        }
    }
}