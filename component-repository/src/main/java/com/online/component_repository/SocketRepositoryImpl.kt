package com.online.component_repository

import com.kproject.simplechat.data.model.ChatMessageEntity
import com.online.component_network.socket.SocketNetwork


import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter

class SocketRepositoryImpl constructor(
    private val socketNetwork: SocketNetwork,
): SocketRepository {
    override suspend fun connect(socketUrl: String) {
        socketNetwork.connect(socketUrl)
    }

    override suspend fun disconnect() {
        socketNetwork.disconnect()
    }

    override fun onConnected()= socketNetwork.onConnected()

    override fun onDisconnected()= socketNetwork.onDisconnected()

    override fun onEvent()  =
        socketNetwork.onEvent()

    override fun onMessage() : Flow<ChatMessageEntity> =
        socketNetwork.onMessage()

    override fun sendMessage(item: ChatMessageEntity)  = socketNetwork.sendMessage(item)

}